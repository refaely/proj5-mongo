"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

##########
# Globals
##

#a multiple to find out the maximum brevet distance
max_brevet_dist = 1.20

#control speedlimits table (taken from https://rusa.org/pages/acp-brevet-control-times-calculator)
control_speedlimits = [(1000, 13.333, 26), (600, 11.428, 28), (400, 15, 30), (300, 15, 32), (200, 15, 32), (0, 15, 34)]

#Special table for 200k and 400k
brevet_max_times_shift = [(200, 10), (400, 20)]

###########################
#determining opening times
def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    
    #checking whether the control_dist_km is bigger than the maximum distance possible
    if int(control_dist_km) > brevet_dist_km * max_brevet_dist:
        return False
    
    #Assigning contorol_dist_km to brever_dist_km if it's in the bound
    if (int(control_dist_km) <= brevet_dist_km * max_brevet_dist and int(control_dist_km) > brevet_dist_km):
        control_dist_km = brevet_dist_km
    
    #Calculating
    open_hours = 0

    for dist, min_speed, max_speed in control_speedlimits:
        if control_dist_km > dist:
            open_hours += (control_dist_km - dist) / max_speed
            control_dist_km = dist

    return arrow.get(brevet_start_time).shift(hours=open_hours).isoformat()


###########################
#determining closing times
def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    
    close_time = arrow.get(brevet_start_time)
    
    #close in 1 hour if control_dist_km is 0 (starting point)
    if control_dist_km == 0:
        return close_time.shift(hours=1).isoformat()

    
    #Assigning contorol_dist_km to brever_dist_km if it's in the bound
    if (int(control_dist_km) <= brevet_dist_km * max_brevet_dist and int(control_dist_km) > brevet_dist_km):
        control_dist_km = brevet_dist_km
    
    #set to correct final endtime in case of special brevet dist
    for brev_dist, min_shift in brevet_max_times_shift:
        if int(control_dist_km) == brev_dist:
            close_time = close_time.shift(minutes=min_shift)

    #Calculation
    close_hours = 0

    for dist, min_speed, max_speed in control_speedlimits:
        if control_dist_km > dist:
            close_hours += (control_dist_km - dist) / min_speed
            control_dist_km = dist
    
    return close_time.shift(hours=close_hours).isoformat()
